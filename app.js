const loginForm = document.getElementById("login-form");
const loginButton = document.getElementById("login-form-submit");

const button = document.querySelector('#likona');
const button2 = document.querySelector('.zinoumeno');

const audio = document.querySelector("audio");
const video = document.querySelector("video");
const arxiko = document.querySelector('.fanero');
const teliko = document.querySelector('.krufo');
const toBorder = document.getElementById('borderaki');

button.addEventListener("click", () => {
    if (audio.paused) {
      
      audio.play();
      video.play();
      arxiko.style.display = 'none';
      teliko.style.display = 'block';
      toBorder.style.display = 'block';
    } else {
      audio.pause();
      video.pause();
    }
  });

  button2.addEventListener("click", () => {
    if (audio.played) {
      
      
      audio.pause();
      video.pause();
      arxiko.style.display = 'block';
      teliko.style.display = 'none';
      toBorder.style.display = 'none';
      
    } else {
      audio.play();
      video.play();
    }
  });

loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    
    const password = loginForm.password.value;

    if (password.toLowerCase() === "nikol") {
        window.location.href = 'zindex.html'
        
    } else {
        window.location.href = 'https://nikolgames.gitlab.io'
    }
})

const myvid = document.querySelector('.myvideo');

    myvid.addEventListener('ended', function(e) {
      // get the active source and the next video source.
      // I set it so if there's no next, it loops to the first one
      const activesource = document.querySelector(".myvideo source.active");
      const nextsource = document.querySelector(".myvideo source.active + source") || document.querySelector(".myvideo source:first-child");

      
      // deactivate current source, and activate next one
      activesource.className = "";
      nextsource.className = "active";
      myvid.classList.toggle('passive');

      // update the video source and play
      myvid.src = nextsource.src;
      myvid.play();
    });
