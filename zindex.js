const button = document.getElementById('star');

const audio = document.querySelector("audio");

button.addEventListener("click", () => {
    if (audio.paused) {
      
      audio.play();
      
      
    } else {
      audio.pause();
      
    }
  });
